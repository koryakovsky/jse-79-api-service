package ru.nlmk.study.jse79.apiservice.api;

import lombok.Data;

import java.util.List;

@Data
public class PrincipalResponse {

    private String principal;

    private List<String> roles;
}
