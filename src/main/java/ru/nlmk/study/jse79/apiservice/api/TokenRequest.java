package ru.nlmk.study.jse79.apiservice.api;

import lombok.Data;

@Data
public class TokenRequest {

    public TokenRequest() {
    }

    public TokenRequest(String token) {
        this.token = token;
    }

    private String token;
}
