package ru.nlmk.study.jse79.apiservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.study.jse79.apiservice.model.Item;
import ru.nlmk.study.jse79.apiservice.repository.ItemRepository;

import java.util.List;

@RestController
@RequestMapping("/avito/item")
public class ItemController {

    private final ItemRepository itemRepository;

    @Autowired
    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @GetMapping("/get/{id}")
    public Item getById(@PathVariable Long id) {
        return itemRepository.findById(id).get();
    }

    @GetMapping("/getAll")
    public List<Item> getAll() {
        return itemRepository.findAll();
    }
}
