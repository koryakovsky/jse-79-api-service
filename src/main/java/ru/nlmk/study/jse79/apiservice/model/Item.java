package ru.nlmk.study.jse79.apiservice.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "item")
public class Item {

    @Id
    @Column(name = "item_id")
    private Long itemId;

    @Column
    private String title;

    @Column(name = "item_link")
    private String itemLink;

    @Column(name = "image_link")
    private String imageLink;

    @Column
    private Integer price;

    @Column(name = "saved_time", columnDefinition = "TIMESTAMP")
    private LocalDateTime savedTime;
}
