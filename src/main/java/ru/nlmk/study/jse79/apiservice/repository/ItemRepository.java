package ru.nlmk.study.jse79.apiservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nlmk.study.jse79.apiservice.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
}
