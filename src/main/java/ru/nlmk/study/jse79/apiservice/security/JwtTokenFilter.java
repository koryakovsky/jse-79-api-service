package ru.nlmk.study.jse79.apiservice.security;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.nlmk.study.jse79.apiservice.api.PrincipalResponse;
import ru.nlmk.study.jse79.apiservice.api.TokenRequest;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class JwtTokenFilter extends OncePerRequestFilter {

    @Value("${jwt.token.header}")
    private String jwtHeader;

    @Value("${jwt.token.prefix}")
    private String jwtPrefix;

    @Value("${auth.service.name}")
    private String authServiceName;

    @Autowired
    private EurekaClient eurekaClient;

    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {

        String token = httpServletRequest.getHeader(jwtHeader);

        if (token == null || !token.startsWith(jwtPrefix)) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        token = token.replace(jwtPrefix, "");

        InstanceInfo authService = eurekaClient.getApplication("authServiceName").getInstances().get(0);

        String authServiceIp = authService.getIPAddr();
        int authServicePort = authService.getPort();

        String url = "http://" + authServiceIp + ":" + authServicePort +"/auth";

        ResponseEntity<PrincipalResponse> response = null;

        try {
            response = restTemplate.postForEntity(url, new TokenRequest(token), PrincipalResponse.class);
        } catch (Exception e) {
            log.error("Authorization failed" ,e);
        }

        if (response != null) {
            PrincipalResponse principalResponse = response.getBody();

            List<SimpleGrantedAuthority> roles = new ArrayList<>();

            for(String role : principalResponse.getRoles()) {
                roles.add(new SimpleGrantedAuthority(role));
            }

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    principalResponse.getPrincipal(), null,  roles);

            SecurityContextHolder.getContext().setAuthentication(authToken);
        } else {
            SecurityContextHolder.clearContext();
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
